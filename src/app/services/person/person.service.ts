import { Injectable } from '@angular/core';
import { Person } from './person';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {ErrorService} from '../common/error.service';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  rootUrl = 'http://localhost:3000';

  constructor(private http: HttpClient, private errorService: ErrorService) { }

  getPersonList(): Observable<Person[]> {
    const url = '/person/list';
    return this.http.post<Person[]>(this.rootUrl + url, {})
      .pipe(
        catchError(this.errorService.handleError<Person[]>('getPersonList', []))
      );
  }

  getPerson(personId: number): Observable<Person> {
    const url = '/person/get';
    return this.http.post<Person>(this.rootUrl + url, {id: personId})
      .pipe(
        catchError(this.errorService.handleError<Person>('getPerson', null))
      );
  }

  updatePerson(person: Person): Observable<Person> {
    const url = '/person/update';
    return this.http.post<Person>(this.rootUrl + url, {id: person.id, updates: person})
      .pipe(
        catchError(this.errorService.handleError<Person>('updatePerson', null))
      );
  }
}
