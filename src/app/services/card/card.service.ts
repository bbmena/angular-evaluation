import { Injectable } from '@angular/core';
import { Card} from './card';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {ErrorService} from '../common/error.service';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  rootUrl = 'http://localhost:3000';

  constructor(private http: HttpClient, private errorService: ErrorService) { }

  getCard(cardId: number): Observable<Card> {
    const url = '/card/get';
    return this.http.post<Card>(this.rootUrl + url, {id: cardId})
      .pipe(
        catchError(this.errorService.handleError<Card>('getCard', null))
      );
  }

  queryCard(personId: number): Observable<Card[]> {
    const url = '/card/query';
    return this.http.post<Card[]>(this.rootUrl + url, {person: personId })
      .pipe(
        catchError(this.errorService.handleError<Card[]>('queryCard', null))
      );
  }

  addCard(personId: number, cardNumber: string): Observable<Card> {
    const url = '/card/add';
    return this.http.post<Card>(this.rootUrl + url, {person_id: personId, card_number: cardNumber})
      .pipe(
        catchError(this.errorService.handleError<Card>('addCard', null))
      );
  }
}
