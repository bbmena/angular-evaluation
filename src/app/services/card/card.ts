export interface Card {
  id: number;
  card_number: string;
  person_id: number;
  balance: number;
}
