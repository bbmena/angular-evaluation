import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../services/person/person.service';
import { CardService } from '../../services/card/card.service';
import { Person } from '../../services/person/person';
import { ActivatedRoute } from '@angular/router';
import {Card} from '../../services/card/card';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private personService: PersonService, private cardService: CardService) { }

  person: Person;
  personId: number;
  cards: Card[];
  cardNumber = '';

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.personId = +params.id;
      this.getPerson(this.personId);
      this.getCards(this.personId);
    });
  }

  getPerson(personId): void {
    this.personService.getPerson(personId)
      .subscribe(person => this.person = person);
  }

  getCards(personId): void {
    this.cardService.queryCard(personId)
      .subscribe(cards => this.cards = cards);
  }

  addCard(): void {
    this.cardService.addCard(this.personId, this.cardNumber).subscribe(response => {
      this.cards.push(response);
      this.cardNumber = '';
    });
  }

}
