import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../services/person/person.service';
import {Person} from '../../services/person/person';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit {

  constructor(private personService: PersonService) { }

  people: Person[];

  ngOnInit() {
    this.getPeople();
  }

  getPeople(): void {
    this.personService.getPersonList()
      .subscribe(people => this.people = people);
  }
}
