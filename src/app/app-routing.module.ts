import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PersonListComponent} from './components/person-list/person-list.component';
import {DetailsComponent} from './components/details/details.component';

const routes: Routes = [
  {path: '', component: PersonListComponent},
  {path: 'detail/:id', component: DetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
